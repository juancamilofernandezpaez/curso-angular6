describe('ventana login', () => {
    it('tiene link entrar correctamente', () => {
        cy.visit('http://localhost:4200/login');
        cy.get('a').should('contain', 'Entrar!');
    });
});